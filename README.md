# USER Lab website template (Jekyll)

This is a template for creating web sites that use the default styling of the [USER Lab](https://userlab.utk.edu) website. It is implemented using Jekyll.

## Running the project

Use the provided dev container.

New pages can be added directly in the `/src` folder. You will need to update site-wide details in `_config.yml`.

## Deploying the project

Included are the docker containers necessary to automate deployment using portainer.

