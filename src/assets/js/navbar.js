/**
 * Handle setting up a navbar that can react to scrolling.
 */

document.addEventListener('DOMContentLoaded', () => {
  const navbar = $('.navbar-main');

  // This should be calculated, but due to animations, this value is often wrong.
  const navbarScrolledHeight = 63;

  // Keep track of whether scrolling occurred due to a link being clicked.
  // This can also occur on initial page load of the hash value is set.
  let scrollingToAnAnchor = !!window.location.hash;
  Array.from(document.querySelectorAll("a[href^='#']")).filter(a => a.hash.length > 1).forEach(
    a => a.addEventListener('click', e => scrollingToAnAnchor = true)
  );

  // Fired when scrolling occurs
  let scrollCheck = () => {
    // Track whether the navbar is scrolled.
    if (navbar.offset().top > 0) {
      navbar.addClass('scrolled');
    } else {
      navbar.removeClass('scrolled');
    }

    // If scrolling to an anchor we need to adjust the scroll otherwise
    // the navbar will cover the destination.
    if (scrollingToAnAnchor) {
      scrollingToAnAnchor = false;
      $(window).scrollTop($(window).scrollTop() - navbarScrolledHeight);
    }
  };
  $(window).on('scroll', scrollCheck);

  // Check the initial state.
  scrollCheck();

  // Set a class tracking whether the nav is expanded.
  $('#main-navbar-links').on('show.bs.collapse', function () {
    $('.nav-avatar', navbar).addClass('nav-expanded');
  });
  $('#main-navbar-links').on('hide.bs.collapse', function () {
    $('.nav-avatar', navbar).removeClass('nav-expanded');
  });

});
