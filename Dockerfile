###############################################################################
# node_build: Populate the node_modules folder
###############################################################################
FROM node:20 as node_build

WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN yarn install --production

###############################################################################
# ruby_build: Build the Jekyll files 
###############################################################################
FROM ruby:3.2 as ruby_build

WORKDIR /usr/src/app

RUN mkdir -p /usr/src/build /usr/src/nginx

# Add rsync, which can be used to move build files in the compose file
RUN apt update && \
    apt install -y rsync && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Populate the gems
COPY Gemfile ./
RUN bundle install

# Setup the vendor files
COPY --from=node_build /usr/src/app/node_modules ./node_modules
COPY setup-vendor-assets.sh ./
RUN ./setup-vendor-assets.sh

# Build the site
COPY _config.yml ./
COPY src ./src
RUN bundle exec jekyll build -d /usr/src/build --trace

# Copy necessary nginx conf file
COPY nginx.conf ../nginx
