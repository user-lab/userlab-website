#!/bin/sh

# Ensure necessary directories exist
rm -rf src/assets/vendor
mkdir -p src/assets/vendor/css src/assets/vendor/js

# Bootstrap
cp node_modules/bootstrap/dist/css/bootstrap.min.css src/assets/vendor/css/bootstrap.min.css
cp node_modules/bootstrap/dist/css/bootstrap.min.css.map src/assets/vendor/css/bootstrap.min.css.map

cp node_modules/bootstrap/dist/js/bootstrap.bundle.min.js src/assets/vendor/js/bootstrap.bundle.min.js
cp node_modules/bootstrap/dist/js/bootstrap.bundle.min.js.map src/assets/vendor/js/bootstrap.bundle.min.js.map

# Font awesome
cp node_modules/\@fortawesome/fontawesome-free/css/all.min.css src/assets/vendor/css/fontawesome.min.css
cp -r node_modules/\@fortawesome/fontawesome-free/webfonts src/assets/vendor/webfonts

# Jquery
cp node_modules/jquery/dist/jquery.min.js src/assets/vendor/js/jquery.min.js
cp node_modules/jquery/dist/jquery.min.map src/assets/vendor/js/jquery.min.map
