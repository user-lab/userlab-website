#!/bin/sh

# Fix directory permissions for gems and node_modules.
sudo chown -R $(id -u):$(id -g) /usr/local/rvm/gems/default
sudo chown -R $(id -u):$(id -g) node_modules

# Install the correct version of Bundler.
if [ -f Gemfile.lock ] && grep "BUNDLED WITH" Gemfile.lock > /dev/null; then
    cat Gemfile.lock | tail -n 2 | grep -C2 "BUNDLED WITH" | tail -n 1 | xargs gem install bundler -v
fi
bundle install

# Setup the vendor assets
yarn install
./setup-vendor-assets.sh
